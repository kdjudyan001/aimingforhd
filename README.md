# CSIT314 Project: Online Prescription System

Try it out here: https://aimingforhd.azurewebsites.net/

## Project structure
- For Boundary components, see the "Boundaries" folder.
- For Controller components, see the "Controllers" folder.
- For Entity components, see the "Entities" folder.

- For unit tests, see the "UnitTest/AimingForHDTest" folder.
    - This folder also contains "Controllers" and "Entities" subfolders, which are structured similarly to the above. 
    - The unit test file has the same name as the file to be tested, with the word "Tests" added at the back.
    - For example, the unit test file corresponding to `Controllers/General/LoginController.cs` is located at `UnitTest/AimingForHDTest/Controllers/General/LoginControllerTests.cs`. 

The content of "Boundaries" and "Controllers" folder are organized based on user types ("Admin", "Doctor", "Patient", "Pharmacist"). 

The subfolder "General" contains pages/controllers that are used by all user types. These include the login page, the home page, login controller, and logout controller.


## Tools/language used
Developed using ASP.NET Core 5.0

- Visual Studio 2019
- Microsoft Azure SQL Database
- C#
- Dapper ORM
- NUnit (for unit test and test-driven development)
- HTML
- CSS
- JavaScript
- Razor code (C#)

﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class PharmacistViewPrescriptionsController : Controller
    {
        /* Return ViewData["PrescriptionList"], a list of all prescriptions in the DB
         */
        [Route("PharmacistViewPrescriptions")]
        public IActionResult ViewPrescriptions()
        {
            //Redirect to Home if not logged in as pharmacist
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "4")
            {
                return Redirect("~/Home");
            }

            List<Prescription> prescriptions = Prescription.GetPrescriptions();
            prescriptions = prescriptions ?? new List<Prescription>();

            prescriptions.ForEach(p => { p.MedicineList = Medicine.GetMedicinesByPrescription(p.PrescriptionId); });

            ViewData["PrescriptionList"] = prescriptions;

            return View("Pharmacist/PharmacistViewPrescriptions");
        }
    }
}

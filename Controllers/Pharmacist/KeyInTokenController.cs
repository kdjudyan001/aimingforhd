﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class KeyInTokenController : Controller
    {
        /* Return ViewData["Prescription"]
         * A Prescription object with the matching prescriptionId
         */
        [Route("KeyInToken")]
        [HttpGet]
        public IActionResult FindPrescriptionByToken(int prescriptionId=-1)
        {
            //Redirect to Home if not logged in as pharmacist
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "4")
            {
                return Redirect("~/Home");
            }

            if(prescriptionId!=-1)
            {
                Prescription prescription = Prescription.GetPrescriptionByToken(prescriptionId);

                if(prescription!=null)
                    prescription.MedicineList = Medicine.GetMedicinesByPrescription(prescription.PrescriptionId);
            
                ViewData["token"] = prescriptionId;
                ViewData["Prescription"] = prescription;
            }

            return View("Pharmacist/KeyInTokenForm");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AimingForHD.Controllers
{
    public class PharmacistUpdatePrescriptionController : Controller
    {
        /* Get current Medicine data from DB
         * Use this info to initialize form values
         */
        [Route("PharmacistUpdatePrescription")]
        public IActionResult RetrieveMed(int medicineId=-1)
        {
            //Redirect to Home if not logged in as pharmacist
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "4")
            {
                return Redirect("~/Home");
            }

            if (medicineId < 0)
            {
                Redirect("Pharmacist/PharmacistViewPrescriptions");
            }                
            else
            {
                Medicine medicine = Medicine.GetMed(medicineId);

                if (medicine != null)
                {
                    ViewData["Medicine"] = medicine;
                }
            }

            return View("Pharmacist/PharmacistUpdatePrescriptionForm");
        }

        /* ViewData["Medicine"] will return a Medicine object with the updated value if update is successful
         * Otherwise, it will return a Medicine object with the old values
         */
        [Route("PharmacistUpdateMedicine")]
        [HttpPost]
        public IActionResult DispenseMed(int medicineId, int amount)
        {
            //Redirect to Home if not logged in as pharmacist
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "4")
            {
                return Redirect("~/Home");
            }

            Medicine medicine = new()
            {
                MedicineId = medicineId,
                Amount = amount
            };

            if(amount>=0)
            {
                Medicine updateMed = Medicine.DispenseMed(medicineId, amount);
                if(updateMed!=null)
                {
                    medicine = updateMed;
                }
            }

            ViewData["Medicine"] = medicine;

            return View("Pharmacist/PharmacistUpdatePrescriptionForm");
        }
    }
}

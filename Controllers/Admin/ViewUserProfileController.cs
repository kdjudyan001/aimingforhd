﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class ViewUserProfileController : Controller
    {
        /* Return list of all user profiles to the view
         * List of all user profiles will be assigned to ViewData["UserProfileList"]
         */
        [Route("ViewUserProfiles")]
        public IActionResult ViewUserProfiles()
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            List<UserProfile> profileList = UserProfile.GetUserProfiles();
            ViewData["UserProfileList"] = profileList;

            return View("Admin/ViewUserProfiles");
        }
    }
}

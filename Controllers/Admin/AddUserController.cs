﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace AimingForHD.Controllers
{
    public class AddUserController : Controller
    {
        /* Receive form post data
         * Return bool via ViewData["Success"]
         * Add user success : ViewData["Success"] = true
         * Add user fails : ViewData["Success"] = false
         * If all parameters are null, ViewData["Success"] = null and it will simply load the form
         * 
         * Username: length 5-50, letters/numbers
         * Password: at least 8 characters
         */
        [Route("AddNewUser")]
        [AcceptVerbs("Get","Post")]
        public IActionResult AddNewUser(string username = null, string password = null,
            string profile = null, string email = null)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }
            else if (HttpContext.Request.Method == "GET")
            {
                return View("Admin/AddUserForm");
            }

            //If all fields are null, load the empty form
            if (username == null && password == null && profile == null && email == null)
                return View("Admin/AddUserForm");
            else
            {
                //Validate user input
                Regex uNameRegex = new Regex("^[(a-zA-Z)0-9]{5,50}$");
                Regex passwordRegex = new Regex("^.{8,}$");

                bool UsernameValid = username != null && uNameRegex.IsMatch(username);
                bool PasswordValid = passwordRegex.IsMatch(password ?? "");
                bool EmailValid = new EmailAddressAttribute().IsValid(email);

                //To be displayed in form in case of error
                User user = new User()
                {
                    Username = username!=null ? username.Trim() : "",
                    Password = password,
                    Email = email !=null ? email.Trim() : "",
                    Profile = new UserProfile() { Profile = profile }
                };
                ViewData["User"] = user;

                //Declare error messages (not assigned to ViewData yet)
                string UsernameError = "Username must be 5-50 characters long and can only contain letters and numbers.";
                string EmailError = "Invalid email address.";
                string PasswordError = "Password must be at least 8 characters long.";
                string errorMessage = "";

                //If input are valid, insert in DB
                if (UsernameValid && PasswordValid && EmailValid)
                {
                    bool isSuccess = Entities.User.AddNewUser(username.Trim(), password, profile, email.Trim());
                    ViewData["Success"] = isSuccess;

                    if (!isSuccess)
                    {
                        errorMessage += "Username might have been used, or the user profile does not exist.";
                    }
                    else
                    {
                        ViewData["User"] = new User();
                    }
                }
                else
                {
                    ViewData["Success"] = false;
                }

                //Assign relevant error messages to ViewData
                errorMessage += UsernameValid ? "" : UsernameError;
                errorMessage += PasswordValid ? "" : PasswordError;
                errorMessage += EmailValid ? "" : EmailError;
                ViewData["ErrorMessage"] = errorMessage;

                return View("Admin/AddUserForm");
            }//end of else
        }//end of function
    }
}

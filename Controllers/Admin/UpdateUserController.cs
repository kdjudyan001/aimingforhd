﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace AimingForHD.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class UpdateUserController : Controller
    {
        /* Receive selection on which user to be updated by the username. 
         * Get info on the selected user's email, profile etc. This info will be assigned to ViewData["UpdateUser"]
         * If for some reason no record on that username is found, redirect to view all users page
         */
        [Route("UpdateUser")]
        [HttpPost]
        public IActionResult RetrieveData(string username)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            User userToUpdate = Entities.User.GetUser(username);

            //If username does not exist, return to view all users
            if (userToUpdate == null)
                return Redirect("~/ViewUsers");

            ViewData["User"] = userToUpdate;

            return View("Admin/UpdateUserForm");
        }

        /* Receive form data
         * Return User object via ViewData["User"] = the current User object that is being edited
         * 
         * Username: length 5-50, letters/numbers
         * Password: at least 8 characters
         * 
         * changePW indicate whether we want to change password. 
         * If changePW=false, value of pw will be ignored
         */
        [Route("UpdateUser/UpdateUserInfo")]
        [HttpPost]
        public IActionResult UpdateUserInfo(string uNameOld, string uNameNew, 
            string pw, string email, string profile, bool changePW=false)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            //Validate user input
            Regex uNameRegex = new Regex("^[(a-zA-Z)0-9]{5,50}$");
            Regex pwRegex = new Regex("^.{8,}$");

            bool UsernameValid = uNameNew!=null && uNameRegex.IsMatch(uNameNew);
            bool PasswordValid = !changePW || pwRegex.IsMatch(pw ?? "");
            bool EmailValid = new EmailAddressAttribute().IsValid(email);

            //Declare error messages (not assigned to ViewData yet)
            string UsernameError = "Username must be 5-50 characters long and can only contain letters and numbers.";
            string EmailError = "Invalid email address.";            
            string PasswordError = "Password must be at least 8 characters long.";
            string message = "";

            User user = new User()
            {
                Username = uNameNew,
                Password = "",
                Email = email,
                Profile = new UserProfile() { Profile = profile }
            };

            //If input are valid, try to update in DB
            if (UsernameValid && PasswordValid && EmailValid)
            {
                user = Entities.User.UpdateUserInfo(uNameOld,
                    uNameNew, pw, email, profile.Trim(), changePW);
                
                if(user!=null)
                    message = "Update success!";
                
                if(user==null) //update unsuccessful
                {
                    message = "Username might have been used by another user, or the user profile does not exist.";

                    user = new User()
                    {
                        Username = uNameOld,
                        Password = "",
                        Email = email,
                        Profile = new UserProfile() { Profile = profile }
                    };
                }
                else if(user!=null && HttpContext.Session.GetString("CurrentUser")==uNameOld)
                {
                    //If user is changing their own username, update session variable accordingly
                    HttpContext.Session.SetString("CurrentUser", uNameNew);
                }                
            }

            //Assign relevant error messages to ViewData
            message += UsernameValid ? "" : UsernameError;
            message += PasswordValid ? "" : PasswordError;
            message += EmailValid ? "" : EmailError;
            ViewData["Message"] = message;
            ViewData["User"] = user;

            return View("Admin/UpdateUserForm");
        }
    }
}

﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class AddUserProfileController : Controller
    {

        /* Receive form post data
         * Add user profile success : ViewData["Success"] = true
         * Add user profile fails : ViewData["Success"] = false
         */
        [AcceptVerbs("Get", "Post")]
        [Route("AddNewProfile")]
        public IActionResult AddNewProfile(string userProfile=null)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }
            else if (HttpContext.Request.Method == "GET")
            {
                return View("Admin/AddUserProfileForm");
            }

            bool insertSuccess;

            if(userProfile!=null)
            {

                if(userProfile.Trim() == "")
                {
                    insertSuccess = false;
                    ViewData["Success"] = insertSuccess;
                    ViewData["ErrorMessage"] = "User Profile cannot be an empty string.";
                }
                else
                {
                    insertSuccess = UserProfile.AddNewProfile(userProfile.Trim());
                    ViewData["Success"] = insertSuccess;
                    
                    if (!insertSuccess) //unique constraint violation
                    {
                        ViewData["ErrorMessage"] =
                            "The User Profile '" + userProfile.Trim() + "' has already existed.";
                    }
                }
            }

            return View("Admin/AddUserProfileForm");
        }
    }
}

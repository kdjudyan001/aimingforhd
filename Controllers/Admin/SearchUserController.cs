﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class SearchUserController : Controller
    {
        /* Return List<User> 
         * ViewData["UserList"] contain a list uf users matching the keyword
         * If not logged in as admin, redirect to Home page
         */
        [HttpGet]
        [Route("SearchUsers")]
        public IActionResult FindUsersByKeyword(string keyword=null)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            ViewData["Keyword"] = keyword;

            if(keyword != null)
                ViewData["UserList"] = Entities.User.GetUsersByKeyword(keyword);

            return View("Admin/SearchUserForm");
        }
    }
}

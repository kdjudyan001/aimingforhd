﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class UpdateUserProfileController : Controller
    {

        /* Receive form post data
         * Add user profile success : ViewData["Success"] = true
         * Add user profile fails : ViewData["Success"] = false
         */
        [HttpPost]
        [Route("UpdateProfile")]
        public IActionResult UpdateProfile(string profileOld, string profileNew=null)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            ViewData["UserProfile"] = profileOld;

            if(profileNew==null)
                return View("Admin/UpdateUserProfileForm");
            else
            {
                if(profileNew.Trim() == "")
                {
                    ViewData["ErrorMessage"] = "Profile cannot be empty.";
                    ViewData["Success"] = false;
                }
                else
                {
                    bool isSuccess = UserProfile.UpdateUserProfile(profileOld, profileNew.Trim());
                    ViewData["UserProfile"] = isSuccess ? profileNew : profileOld;
                    ViewData["Success"] = isSuccess;
                }
            }

            return View("Admin/UpdateUserProfileForm");
        }
    }
}

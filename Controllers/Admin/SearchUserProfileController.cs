﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class SearchUserProfileController : Controller
    {
        /* Display form to search user profiles by keyword
         * ViewData["UserProfileList"] contain a list uf users matching the keyword
         * ViewData["Keyword"] contain the keyword that was just used to find the user profiles
         * If not logged in as admin, redirect to Home page
         * If keyword=null (e.g. when first loading the page), the page will display no users
         */
        [HttpGet]
        [Route("SearchUserProfiles")]
        public IActionResult FindProfilesByKeyword(string keyword=null)
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            ViewData["Keyword"] = keyword;

            if(keyword != null)
                ViewData["ProfileList"] = Entities.UserProfile.GetProfilesByKeyword(keyword);

            return View("Admin/SearchUserProfileForm");
        }
    }
}

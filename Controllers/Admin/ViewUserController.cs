﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AimingForHD.Controllers
{
    /* Display all users 
     * List of users will be assigned to ViewData["UserList"]
     */
    public class ViewUserController : Controller
    {
        [Route("ViewUsers")]
        public IActionResult ViewUsers()
        {
            //Redirect to Home if not logged in as admin
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "1")
            {
                return Redirect("~/Home");
            }

            List<User> userList = Entities.User.GetAllUsers();
            ViewData["UserList"] = userList;

            return View("Admin/ViewUsers");
        }

    }
}

﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace AimingForHD.Controllers
{
    public class AddPrescriptionController : Controller
    {
        /* Return ViewData["Prescription"], the prescription that was just inserted into the database
         * If fail to insert, ViewData["Prescription"] = null
         */
        [Route("AddNewPrescription")]
        [AcceptVerbs("GET","POST")]
        public IActionResult AddNewPrescription(string doctor="", string patient="")
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }
            else if (HttpContext.Request.Method == "GET")
            {
                return View("Doctor/AddPrescriptionForm");
            }

            ViewData["Patient"] = patient;

            doctor ??= "";
            patient ??= "";

            if(doctor.Trim()!="" && patient.Trim()!="")
            {
                Prescription newPrescription = Prescription.AddNewPrescription(doctor.Trim(), patient.Trim());

                if(newPrescription!=null)
                {
                    ViewData["Prescription"] = newPrescription;
                    SendQR(newPrescription.PrescriptionId, newPrescription.Patient.Email);
                }
                else
                {
                    ViewData["Status"] = "An error occurred. Check if the patient username is correct.";
                }
            }
            
            return View("Doctor/AddPrescriptionForm");
        }

        /* If fail to add new Medicine, ViewData["Medicine"] will contain a default Medicine
         * The default medicine will only contain the current prescriptionId.
         * If success, ViewData["Medicine"] will contain the newly inserted Medicine
         */
        [Route("AddNewMedicine")]
        [HttpPost]
        public IActionResult AddNewMedicine(string name, int amount, int prescriptionId)
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }

            name ??= "";

            if(name.Trim()!="" && amount>0)
            {
                Medicine newMedicine = Medicine.AddNewMedicine(name.Trim(), amount, prescriptionId);

                if (newMedicine == null)
                {
                    ViewData["Medicine"] = new Medicine() { PrescriptionId = prescriptionId };
                    ViewData["Status"] = "An error occurred.";
                }
                else
                {
                    ViewData["Medicine"] = newMedicine;
                    ViewData["Status"] = "Successfully added a medicine to prescription.";
                }
            }
            else 
            {
                string error = "";
                if (name.Trim() == "")
                    error += "Name cannot be empty.\n";
                if (amount <= 0)
                    error += "Amount must be greater than 0.";
                ViewData["Status"] = error;
                ViewData["Medicine"] = new Medicine() { PrescriptionId = prescriptionId };
            }
            return View("Doctor/AddPrescriptionForm");
        }

        [NonAction]
        public void SendQR(int prescriptionId, string email)
        {
            var senderEmail = "csit314aimingforhd@gmail.com";
            var password = "#Csit314";
            
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderEmail, password)
            };

            using var mess = new MailMessage(senderEmail, email)
            {
                From = new MailAddress(senderEmail, "CSIT314 Online Prescription System"),
                Subject = "Prescription Token",
                Body = "Your prescription token is " + prescriptionId + "."
            };
            QRCodeGenerator _qrCode = new();
            QRCodeData _qrCodeData = _qrCode.CreateQrCode(prescriptionId.ToString(), QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new(_qrCodeData);
            Bitmap QR = qrCode.GetGraphic(20);
            QR.Save("tokenqr.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);

            Attachment item = new("tokenqr.jpeg");
            mess.Attachments.Add(item);
            smtp.Send(mess);

            QR.Dispose();
        }

    }
}

﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class SearchPrescriptionController : Controller
    {
        /* Return ViewData["PrescriptionList"], a list of prescriptions matching the keyword
         */
        [HttpGet]
        [Route("SearchPrescription")]
        public IActionResult FindPrescByKeyword(string keyword)
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }

            if(keyword != null)
            { 
                List<Prescription> prescriptions = Prescription.GetPrescriptionsByKeyword(keyword.Trim());

                if(prescriptions!=null)
                {
                    prescriptions.ForEach(p => { p.MedicineList = Medicine.GetMedicinesByPrescription(p.PrescriptionId); });
                }

                ViewData["keyword"] = keyword;
                ViewData["PrescriptionList"] = prescriptions;
            }

            return View("Doctor/SearchPrescriptionForm");
        }
    }
}

﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class ViewPrescriptionController : Controller
    {
        /* Return ViewData["PrescriptionList"], a list of prescriptions 
         */
        [Route("ViewPrescriptions")]
        public IActionResult ViewPrescriptions()
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }

            List<Prescription> prescriptions = Prescription.GetPrescriptions();
            prescriptions = prescriptions ?? new List<Prescription>();

            prescriptions.ForEach(p => { p.MedicineList = Medicine.GetMedicinesByPrescription(p.PrescriptionId); });

            ViewData["PrescriptionList"] = prescriptions;

            return View("Doctor/ViewPrescriptions");
        }
    }
}

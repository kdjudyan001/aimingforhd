﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AimingForHD.Controllers
{
    public class UpdatePrescriptionController : Controller
    {
        /* Get current Medicine data from DB
         * Use this info to initialize form values
         */
        [Route("UpdatePrescription")]
        public IActionResult RetrieveMed(int medicineId=-1)
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }

            if (medicineId < 0)
            {
                Redirect("Doctor/ViewPrescriptions");
            }                
            else
            {
                ViewData["Medicine"] = Medicine.GetMed(medicineId);
            }

            return View("Doctor/UpdatePrescriptionForm");
        }

        /* ViewData["Medicine"] will return a Medicine object with the updated value if update is successful
         * Otherwise, it will return a Medicine object with the old values
         * ViewData["Status"] will contain message whether update is successful/invalid user input etc.
         */
        [Route("UpdateMedicine")]
        [HttpPost]
        public IActionResult UpdateMed(int medicineId, string name, int amount)
        {
            //Redirect to Home if not logged in as doctor
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "2")
            {
                return Redirect("~/Home");
            }

            Medicine medicine = new()
            {
                MedicineId = medicineId,
                Name = name,
                Amount = amount
            };

            string message = "";

            if(amount<0)
            {
                message += "Amount cannot be negative.\n";
            }

            if(name==null || name.Trim() == "")
            {
                message += "Name cannot be empty.\n";
            }

            if(amount>=0 && name !=null && name.Trim()!="")
            {
                Medicine updateMed = Medicine.UpdateMed(medicineId, name.Trim(), amount);
                if(updateMed==null)
                {
                    message += "An error occurred. Failed to update.\n";
                }
                else
                {
                    message += "Update success! \n";
                    medicine = updateMed;
                }
            }
            ViewData["Status"] = message;

            ViewData["Medicine"] = medicine;
            return View("Doctor/UpdatePrescriptionForm");
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

namespace AimingForHD.Controllers
{
    public class LogoutController : Controller
    {
        /* Clear all session variables, then redirect to login page
         */
        [HttpPost]
        [Route("Logout")]
        public IActionResult logout()
        {
            HttpContext.Session.Clear(); 
            return View("General/LoginForm");
        }
    }
}

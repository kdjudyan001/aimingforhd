﻿using Microsoft.AspNetCore.Mvc;
using AimingForHD.Entities;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using AimingForHD.Helpers;
using Dapper;

namespace AimingForHD.Controllers
{
    public class LoginController : Controller
    {
        /* Display Home page
         * If Session variable "IsLoggedIn" is not "true", redirect to login page
         */
        [Route("Home")]
        public IActionResult Home()
        {
            //Redirect to login if not logged in 
            if (HttpContext.Session.GetString("IsLoggedIn") != "true")
            {
                return Redirect("~/Login");
            }

            return View("General/HomePage");
        }

        /* Receive username and password, check whether the corresponding record exists in database
         * If yes, set the relevant session variables and return ViewData["LoginSuccess"] = true to the view
         * If not, set the relevant session variables and return ViewData["LoginSuccess"] = true to the view
         */
        [AcceptVerbs("GET","POST")]
        [Route("")]
        [Route("Login")]
        [Route("Login/Validate")]
        public IActionResult ValidateLogin(string username, string password)
        {
            if(HttpContext.Request.Method=="GET")
            {
                return View("General/LoginForm");
            }

            //user will be null if no record matching username & password is found in database
            User user = Entities.User.FindUser(username, password);

            if(user != null)
            {
                //Set session variables
                HttpContext.Session.SetString("CurrentUserID", user.Id.ToString());
                HttpContext.Session.SetString("CurrentUser", user.Username);
                HttpContext.Session.SetString("CurrentProfile", user.Profile.Id.ToString());
                HttpContext.Session.SetString("IsLoggedIn", "true");

                //Tell boundary that login is successful
                ViewData["LoginSuccess"] = true;

                return Redirect("~/Home");
            }
            else 
            {
                //Set session variables
                HttpContext.Session.SetString("IsLoggedIn", "false");

                //Tell boundary that login failed
                ViewData["LoginSuccess"] = false;

                return View("General/LoginForm");
            }

        }
        
    }
}

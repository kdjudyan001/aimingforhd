﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace AimingForHD.Controllers
{
    public class EditPatientController : Controller
    {
        /* Receive selection on which user to be updated by the username. 
         * Get info on the selected user's email, profile etc. This info will be assigned to ViewData["User"]
         * If for some reason no record on that username is found, redirect to view all users page
         */
        [Route("EditPatient")]
        public IActionResult RetrieveData(string username)
        {
            //Redirect to Home if not logged in as patient
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "3" || 
                HttpContext.Session.GetString("CurrentUser") != username)
            {
                return Redirect("~/Home");
            }

            User userToUpdate = Entities.User.GetUser(HttpContext.Session.GetString("CurrentUser"));

            //If username does not exist, return to home page
            if (userToUpdate == null)
                return Redirect("~/Home");

            ViewData["User"] = userToUpdate;

            return View("Patient/EditPatientForm");
        }

        /* Receive form data
         * Return User object via ViewData["User"] = the current User object that is being edited
         * 
         * Username: length 5-50, letters/numbers
         * Password: at least 8 characters
         * 
         * changePW indicate whether we want to change password. 
         * If changePW=false, value of pw will be ignored
         */
        [Route("EditPatient/UpdateUserInfo")]
        [HttpPost]
        public IActionResult UpdateUserInfo(string uNameOld, string uNameNew, 
            string pw, string email, bool changePW=false)
        {
            //Redirect to Home if not logged in as patient
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "3" ||
                HttpContext.Session.GetString("CurrentUser") != uNameOld)
            {
                return Redirect("~/Home");
            }

            //Validate user input
            Regex uNameRegex = new("^[(a-zA-Z)0-9]{5,50}$");
            Regex pwRegex = new("^.{8,}$");

            bool UsernameValid = uNameNew != null && uNameRegex.IsMatch(uNameNew);
            bool PasswordValid = !changePW || pwRegex.IsMatch(pw ?? "");
            bool EmailValid = new EmailAddressAttribute().IsValid(email);

            //Declare error messages (not assigned to ViewData yet)
            string UsernameError = "Username must be 5-50 characters long and can only contain letters and numbers.";
            string EmailError = "Invalid email address.";
            string PasswordError = "Password must be at least 8 characters long.";
            string message = "";

            User user = new()
            {
                Username = uNameNew,
                Password = "",
                Email = email
            };

            //If input are valid, try to update in DB
            if (UsernameValid && PasswordValid && EmailValid)
            {
                user = Entities.User.UpdateUserInfo(HttpContext.Session.GetString("CurrentUser"), uNameNew, pw, email, changePW);

                if (user != null)
                    message = "Update success!";

                if (user == null) //update unsuccessful
                {
                    message = "Username might have been used by another user.";

                    user = new User()
                    {
                        Username = uNameOld,
                        Password = "",
                        Email = email
                    };
                }
                else if (user != null && HttpContext.Session.GetString("CurrentUser") == uNameOld)
                {
                    //If user is changing their own username, update session variable accordingly
                    HttpContext.Session.SetString("CurrentUser", uNameNew);
                }
            }

            //Assign relevant error messages to ViewData
            message += UsernameValid ? "" : UsernameError;
            message += PasswordValid ? "" : PasswordError;
            message += EmailValid ? "" : EmailError;
            ViewData["Message"] = message;
            ViewData["User"] = user;

            return View("Patient/EditPatientForm");
        }
    }
}

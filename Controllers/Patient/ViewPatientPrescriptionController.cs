﻿using AimingForHD.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AimingForHD.Controllers
{
    public class ViewPatientPrescriptionController : Controller
    {
        /* Return ViewData["PrescriptionList"], a list of prescriptions matching the keyword
         */
        [Route("ViewPatientPrescriptions")]
        [HttpPost]
        public IActionResult ViewPrescriptions(int patientId)
        {
            //Redirect to Home if not logged in as patient
            if (HttpContext.Session.GetString("IsLoggedIn") != "true" ||
                HttpContext.Session.GetString("CurrentProfile") != "3")
            {
                return Redirect("~/Home");
            }

            List<Prescription> prescriptions = Prescription.GetPrescriptions(patientId);

            if(prescriptions != null)
                prescriptions.ForEach(p => { p.MedicineList = Medicine.GetMedicinesByPrescription(p.PrescriptionId); });

            ViewData["PrescriptionList"] = prescriptions;

            return View("Patient/ViewPatientPrescriptions");
        }
    }
}

﻿using Microsoft.Data.SqlClient;

namespace AimingForHD.Helpers
{
    public class DbConnection
    {
        private static string connectionString = "Data Source=tcp:aimingforhddbserver.database.windows.net,1433;Initial Catalog=AimingForHD_db;User Id=AimForHD@aimingforhddbserver;Password=#csit314";
        private static string testConnectionString = "Data Source=tcp:aimingforhddbserver.database.windows.net,1433;Initial Catalog = AimingForHDTest_db; User Id = AimForHD@aimingforhddbserver;Password=#csit314";
        private static bool isTest = false;

        public static SqlConnection Open()
        {
            return new SqlConnection(isTest ? testConnectionString : connectionString);
        }

        public static void IsTest(bool isTest)
        {
            DbConnection.isTest = isTest;
        }
    }
}

﻿using AimingForHD.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Text;

namespace AimingForHDTest.Controllers
{
    class LogoutControllerTests
    {
        [SetUp]
        public void Setup()
        {

        }

        //Test that session variable "IsLoggedIn" is cleared
        [Test]
        public void TestLoginSessionCleared()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LogoutController controller = new LogoutController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.logout();

            Assert.AreEqual(null, controller.ControllerContext.HttpContext.Session.GetString("IsLoggedIn"));
        }

        //Test that session variable "CurrentProfile" is cleared
        [Test]
        public void TestProfileSessionCleared()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LogoutController controller = new LogoutController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.logout();

            Assert.AreEqual(null, controller.ControllerContext.HttpContext.Session.GetString("CurrentProfile"));
        }

        //Test that session variable "CurrentUserID" is cleared
        [Test]
        public void TestUserIDSessionCleared()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockSession["CurrentUserID"] = "100";
            mockSession["CurrentUser"] = "Admin1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LogoutController controller = new LogoutController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.logout();

            Assert.AreEqual(null, controller.ControllerContext.HttpContext.Session.GetString("CurrentUserID"));
        }

        //Test that session variable "CurrentUser" is cleared
        [Test]
        public void TestUserSessionCleared()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockSession["CurrentUserID"] = "100";
            mockSession["CurrentUser"] = "Admin1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LogoutController controller = new LogoutController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.logout();

            Assert.AreEqual(null, controller.ControllerContext.HttpContext.Session.GetString("CurrentUser"));
        }

        //Go to login page after all sessions are cleared
        [Test]
        public void TestLogoutRedirectToLogin()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockSession["CurrentUserID"] = "100";
            mockSession["CurrentUser"] = "Admin1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LogoutController controller = new LogoutController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.logout() as ViewResult;

            Assert.AreEqual("General/LoginForm",result.ViewName);
        }


    }
}

﻿using AimingForHD.Controllers;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AimingForHDTest.Controllers
{
    class LoginControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display Home page if logged in
        [Test]
        public void TestHomeLoggedIn()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult) controller.Home();
            Assert.AreEqual("General/HomePage", result.ViewName);
        }

        //Redirect if not authorized
        [Test]
        public void TestHomeNotLoggedInRedirect()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.Home();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Check if session variables have been set
        [Test]
        public void TestLoginSuccessSession()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.ValidateLogin("Admin1", "Admin123");

            Assert.AreEqual("true", controller.ControllerContext.HttpContext.Session.GetString("IsLoggedIn"));
        }

        //Check if session variables have been set
        [Test]
        public void TestLoginSuccessSession2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.ValidateLogin("Admin1", "Admin123");

            Assert.AreEqual("Admin1", controller.ControllerContext.HttpContext.Session.GetString("CurrentUser"));
        }
        //Check if session variables have been set
        [Test]
        public void TestLoginSuccessSession3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.ValidateLogin("Admin1", "Admin123");

            Assert.AreEqual("100", controller.ControllerContext.HttpContext.Session.GetString("CurrentUserID"));
        }
        //Check if session variables have been set
        [Test]
        public void TestLoginSuccessSession4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.ValidateLogin("Admin1", "Admin123");

            Assert.AreEqual("1", controller.ControllerContext.HttpContext.Session.GetString("CurrentProfile"));
        }

        //Check if session variables have been set
        [Test]
        public void TestLoginFailSession()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            controller.ValidateLogin("", "");

            Assert.AreNotEqual("true", controller.ControllerContext.HttpContext.Session.GetString("IsLoggedIn"));
        }

        //Redirect to Home page if successful
        [Test]
        public void TestLoginSuccessRedirect()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.ValidateLogin("Admin1", "Admin123"));
        }

        //Display login page
        [Test]
        public void TestDisplayLoginForm()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");


            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            ViewResult result = (ViewResult)controller.ValidateLogin(null,null);

            Assert.AreEqual("General/LoginForm", result.ViewName);
        }

        //Reload login form if login fail
        [Test]
        public void TestLoginFailReloadForm()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");


            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            ViewResult result = (ViewResult)controller.ValidateLogin("Admin1", "adm1");

            Assert.AreEqual("General/LoginForm", result.ViewName);
        }

        //Check view data for failed login
        [Test]
        public void TestLoginFailViewData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            LoginController controller = new LoginController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            ViewResult result = (ViewResult)controller.ValidateLogin("Admin1", "adm");

            Assert.AreNotEqual(true, result.ViewData["LoginSuccess"]);
        }

    }
}

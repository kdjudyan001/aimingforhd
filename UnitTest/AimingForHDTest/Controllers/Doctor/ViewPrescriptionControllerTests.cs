﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class ViewPrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as doctor
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPrescriptionController controller = new ViewPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.ViewPrescriptions());
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPrescriptionController controller = new ViewPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewPrescriptions();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestViewAllPrescriptions()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPrescriptionController controller = new ViewPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions();
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestViewAllPrescriptionsMedicineList()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPrescriptionController controller = new ViewPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions();
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];
            Assert.That(() => prescriptions.TrueForAll(p => p.MedicineList is List<Medicine>));
        }

    }
}

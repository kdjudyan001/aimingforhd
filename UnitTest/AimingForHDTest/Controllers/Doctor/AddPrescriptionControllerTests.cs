﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;

namespace AimingForHDTest.Controllers
{
    class AddPrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE [User] SET Email='csit314aimingforhd@gmail.com' WHERE Username='Patient1';");
            }
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE [User] SET Email='patient1@fakemail.com' WHERE Username='Patient1';");
                db.Query("DELETE FROM [Prescription] WHERE PrescriptionId>100003;");
                db.Query("DELETE FROM [Medicine] WHERE MedicineId>1010;");
            }
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as doctor
        [Test]
        public void TestAuthorizedAddPrescription()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.AddNewPrescription("", ""));
        }

        //Redirect if not logged in as doctor
        [Test]
        public void TestUnauthorizedAddPrescription()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewPrescription("", "");
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not logged in as doctor
        [Test]
        public void TestUnauthorizedAddPrescription2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3"; //not a doctor
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewPrescription("", "");
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestAuthorizedAddMedicine()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.AddNewMedicine("Med5",10,100000));
        }

        //Redirect if not logged in as doctor
        [Test]
        public void TestUnauthorizedAddMedicine()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewMedicine("Med5", 10, 100000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not logged in as doctor
        [Test]
        public void TestUnauthorizedAddMedicine2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3"; //not a doctor
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewMedicine("Med5", 10, 100000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestAddPrescription()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            ViewResult result = (ViewResult)controller.AddNewPrescription("Doctor1", "Patient1");

            Assert.IsInstanceOf<Prescription>(result.ViewData["Prescription"]);
        }

        [Test]
        public void TestAddPrescriptionInvalidPatient()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.AddNewPrescription("Doctor1", "Admin1");//not a patient

            Assert.IsNull(result.ViewData["Prescription"]);
        }

        [Test]
        public void TestAddPrescriptionInvalidDoctor()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.AddNewPrescription("Admin2", "Patient1");//not a doctor

            Assert.IsNull(result.ViewData["Prescription"]);
        }

        [Test]
        public void TestSendQR()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.DoesNotThrow(() => controller.SendQR(100, "csit314aimingforhd@gmail.com"));
        }

        [Test]
        public void TestSendQRInvalidEmail()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.Throws<ArgumentException>(() => controller.SendQR(100, ""));
        }

        [Test]
        public void TestAddMedicine()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            AddPrescriptionController controller = new AddPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            ViewResult result = (ViewResult)controller.AddNewMedicine("Med5",10,100000);

            Assert.IsInstanceOf<Medicine>(result.ViewData["Medicine"]);
        }


    }
}

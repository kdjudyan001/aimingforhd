﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class UpdatePrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true); //Connect to test DB
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE Medicine SET Name='Med2', Amount=2 WHERE MedicineId=1001");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestAuthorizedRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveMed(1000));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorizedRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.RetrieveMed(1000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<Medicine>((controller.RetrieveMed(1000) as ViewResult).ViewData["Medicine"]);
        }

        [Test]
        public void TestRetrieveMedNotFound()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveMed(10));
        }

        [Test]
        public void TestRetrieveMedNotFound2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsNull((controller.RetrieveMed(10) as ViewResult).ViewData["Medicine"]);
        }

        [Test]
        public void TestAuthorizedUpdateMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.UpdateMed(1001,"Med2",10));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorizedUpdateMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.UpdateMed(1001, "Med2", 10));
        }


        [Test]
        public void TestUpdateMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdatePrescriptionController controller = new UpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Medicine expected = new()
            {
                MedicineId = 1001,
                Name = "Med20",
                Amount = 10
            };

            Medicine actual = (Medicine)(controller.UpdateMed(1001, "Med20", 10) as ViewResult).ViewData["Medicine"];

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Name == actual.Name &&
                expected.Amount == actual.Amount
            );
        }

    }
}

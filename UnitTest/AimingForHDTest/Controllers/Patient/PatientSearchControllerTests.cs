﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class PatientSearchControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as patient
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindMyPrescriptions("100000",104));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindMyPrescriptions("100000",104);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestFindPrescription()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("100",104);
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("doctor1",104);
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("doc", 104);
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("100", 100); //not a patient
            Assert.IsNull(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription5()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("100", 104);
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];

            //Check that all prescriptions returned belong to the specified patient
            Assert.IsTrue(prescriptions.TrueForAll(p => p.Patient.Id == 104));
        }

        [Test]
        public void TestFindPrescription6()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PatientSearchController controller = new PatientSearchController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindMyPrescriptions("100", 104);
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];

            //Check that all prescriptions returned belong to the specified patient
            Assert.IsTrue(prescriptions.TrueForAll(p => p.Patient.Id == 104));
        }
    }
}

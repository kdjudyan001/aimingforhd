﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class ViewPatientPrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as doctor
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.ViewPrescriptions(104));
        }

        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewPrescriptions(104);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //doctor
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewPrescriptions(104);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestViewAllPrescriptions()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions(104);
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestViewAllPrescriptions2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions(100); //not a patient
            Assert.IsNull(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestViewAllPrescriptions3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions(104);
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];

            Assert.IsTrue(prescriptions.TrueForAll(p=> p.Patient.Id==104));
        }

        [Test]
        public void TestViewAllPrescriptionsMedicineList()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewPatientPrescriptionController controller = new ViewPatientPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewPrescriptions(104);
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];
            Assert.IsInstanceOf<List<Medicine>>(prescriptions.ToArray()[0].MedicineList);
        }

    }
}

﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace AimingForHDTest.Controllers
{
    class EditPatientControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            using(var db = DbConnection.Open())
            {
                db.Query("UPDATE [User] SET Username='Patient2', " +
                    "Password=HASHBYTES('SHA2_512',CONVERT(VARCHAR(50),'Patient123')), " +
                    "Email='patient2@fakemail.com' WHERE Username='Patient20';");
            }
            DbConnection.IsTest(false);
        }

        //Display info of the user to be updated, if logged in as patient
        [Test]
        public void TestAuthorizedRetrieveData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveData("Patient1"));
        }

        //Redirect if not authorized as patient
        [Test]
        public void TestUnauthorizedRetrieveData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("Patient1"));
        }

        //Redirect if not authorized as patient
        [Test]
        public void TestUnauthorizedRetrieveData2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Should redirect since Patient1 != Patient2
            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("Patient2"));
        }

        //Redirect if not authorized as patient
        [Test]
        public void TestUnauthorizedRetrieveData3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Should redirect since Patient1 != Patient2
            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("Patient2"));
        }

        //Redirect if not authorized as patient
        [Test]
        public void TestUnauthorizedRetrieveData4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "false";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Should redirect since Patient1 != Patient2
            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("Patient2"));
        }

        //If authorized to update user, display update form
        [Test]
        public void TestAuthorizedUpdatePatient()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.UpdateUserInfo("Patient2","Patient20","","patient2@fakemail.com"));
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdatePatient()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.UpdateUserInfo("", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdatePatient2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Old username must be the same as session CurrentUser
            var result = controller.UpdateUserInfo("Patient2", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdatePatient3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Old username must be the same as session CurrentUser
            var result = controller.UpdateUserInfo("Patient2", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdatePatient4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //not a patient
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            //Old username must be the same as session CurrentUser
            var result = controller.UpdateUserInfo("Patient2", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestUpdatePatientViewData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.UpdateUserInfo("Patient2", "", "", "", false);

            Assert.IsInstanceOf<User>(result.ViewData["User"]);
        }

        [Test]
        public void TestUpdatePatientViewData2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            User expectedUser = new User()
            {
                Username = "Patient20",
                Password = null,
                Email = "patient2new@mail.com"
            };

            var result = (ViewResult)controller.UpdateUserInfo("Patient2", expectedUser.Username,
                null, expectedUser.Email, false);
            User actualUser = (User)result.ViewData["User"];

            Assert.That(actualUser.Username == expectedUser.Username
                && actualUser.Email == expectedUser.Email);
        }

        //Update with existing username should fail
        [Test]
        public void TestUpdatePatientViewData3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            User expectedUser = new User()
            {
                Username = "Patient2",
                Password = null,
                Email = "patient2new@mail.com"
            };

            var result = (ViewResult)controller.UpdateUserInfo("Patient2", "Patient1",
                null, expectedUser.Email, false);
            User actualUser = (User)result.ViewData["User"];

            Assert.That(actualUser.Username == expectedUser.Username
                && actualUser.Email == expectedUser.Email);
        }


        [Test]
        public void TestUpdatePatientViewData4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3";
            mockSession["CurrentUser"] = "Patient2";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            EditPatientController controller = new EditPatientController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            User expectedUser = new User()
            {
                Username = "Patient20",
                Password = "12345678",
                Email = "patient2new@mail.com"
            };

            var result = (ViewResult)controller.UpdateUserInfo("Patient2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, true);
            User actualUser = (User)result.ViewData["User"];

            Assert.That(actualUser.Username == expectedUser.Username
                && actualUser.Email == expectedUser.Email);
        }

    }
}

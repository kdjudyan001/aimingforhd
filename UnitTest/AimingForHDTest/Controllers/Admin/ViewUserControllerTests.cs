﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class ViewUserControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as admin
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserController controller = new ViewUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.ViewUsers());
        }

        //Redirect if not admin
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserController controller = new ViewUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewUsers();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not admin
        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //not admin
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserController controller = new ViewUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewUsers();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestViewAllUsersViewData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserController controller = new ViewUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.ViewUsers();
            Assert.IsInstanceOf<List<User>>(result.ViewData["UserList"]);
        }
        
    }
}

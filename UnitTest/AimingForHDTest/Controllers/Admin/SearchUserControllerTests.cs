﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class SearchUserControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display search page if logged in as admin
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserController controller = new SearchUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindUsersByKeyword(""));
        }

        //Redirect if not authorized as admin
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserController controller = new SearchUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindUsersByKeyword("");
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not authorized as admin
        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //not admin
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserController controller = new SearchUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindUsersByKeyword("");
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestFindUsersByKeyword()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserController controller = new SearchUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindUsersByKeyword("admin");
            Assert.That(result.ViewData["UserList"] is List<User>);
        }

        [Test]
        public void TestFindUsersByKeyword2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserController controller = new SearchUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindUsersByKeyword("####");
            Assert.IsTrue((result.ViewData["UserList"] as List<User>).Count==0);
        }

    }
}

using AimingForHD.Controllers;
using AimingForHD.Entities;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using Moq;
using Microsoft.AspNetCore.Http;
using AimingForHD.Helpers;

namespace AimingForHDTest.Controllers
{
    public class ViewUserProfileControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Should be able to access page if logged in as admin
        [Test]
        public void TestAuthorizedProfile()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";

            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            ViewUserProfileController controller = new ViewUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.ViewUserProfiles());
            //ViewResult result = (ViewResult)controller.ViewUserProfiles();
            //Assert.AreEqual("ViewUserProfiles.cshtml", result.ViewName);
        }

        //Should redirect if not logged in as admin
        [Test]
        public void TestUnauthorizedProfile()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserProfileController controller = new ViewUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewUserProfiles();

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not admin
        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //not admin
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserProfileController controller = new ViewUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.ViewUserProfiles();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Should return a list of UserProfile
        [Test]
        public void TestViewAllUserProfilesViewData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            ViewUserProfileController controller = new ViewUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.ViewUserProfiles();

            Assert.IsInstanceOf<List<UserProfile>>(result.ViewData["UserProfileList"]);
        }

    }
}
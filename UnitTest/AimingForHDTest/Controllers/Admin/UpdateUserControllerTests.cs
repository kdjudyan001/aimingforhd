﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace AimingForHDTest.Controllers
{
    class UpdateUserControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            using(var db = DbConnection.Open())
            {
                db.Query("UPDATE [User] SET Username='Admin2', " +
                    "Password=HASHBYTES('SHA2_512',CONVERT(VARCHAR(50),'Admin123')), " +
                    "Email='admin2@fakemail.com', Profile=1 WHERE Username='Admin20';");
                db.Query("UPDATE [User] SET Username='Patient2', " +
                    "Password=HASHBYTES('SHA2_512',CONVERT(VARCHAR(50),'Patient123')), " +
                    "Email='patient2@fakemail.com', Profile=3 WHERE Username='Patient20';");
            }
            DbConnection.IsTest(false);
        }

        //Display info of the user to be updated, if logged in as admin
        [Test]
        public void TestAuthorizedRetrieveData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveData("Admin1"));
        }

        //Redirect if not authorized as admin
        [Test]
        public void TestUnauthorizedRetrieveData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("Admin1"));
        }

        //Display info of the user to be updated, if logged in as admin
        [Test]
        public void TestRetrieveData()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<User>((controller.RetrieveData("Admin1") as ViewResult).ViewData["User"]);
        }

        /* Display info of the user to be updated, if logged in as admin
         * If the username cannot be found in the database, something is wrong and redirect to view users page
         */
        [Test]
        public void TestRetrieveData2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.RetrieveData("NotAUser"));
        }


        //If authorized to update user, return to update form
        [Test]
        public void TestAuthorizedUpdateUser()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.UpdateUserInfo("","","","",""));
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdateUser()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.UpdateUserInfo("", "", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect to Home if not authorized to update user
        [Test]
        public void TestUnauthorizedUpdateUser2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "2"; //not admin
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.UpdateUserInfo("", "", "", "", "", false);

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestUpdateUser()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.UpdateUserInfo("Admin2", "", "", "", "", false);

            Assert.IsInstanceOf<User>(result.ViewData["User"]);
        }

        [Test]
        public void TestUpdateUser2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            User expectedUser = new User()
            {
                Username = "Admin20",
                Password = null,
                Email = "adminnewmail@fakemail.com",
                Profile = new UserProfile() { Profile = "Admin" }
            };

            var result = (ViewResult)controller.UpdateUserInfo("Admin2", expectedUser.Username,
                null, expectedUser.Email, expectedUser.Profile.Profile, false);
            User actualUser = (User)result.ViewData["User"];

            Assert.That(actualUser.Username == expectedUser.Username
                && actualUser.Email == expectedUser.Email
                && actualUser.Profile.Profile == expectedUser.Profile.Profile);
        }

        [Test]
        public void TestUpdateUser3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserController controller = new UpdateUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            User expectedUser = new User()
            {
                Username = "Admin20",
                Password = "NEWPASSWORD1",
                Email = "adminnewmail@fakemail.com",
                Profile = new UserProfile() { Profile = "Admin" }
            };

            var result = (ViewResult)controller.UpdateUserInfo("Admin2", expectedUser.Username,
                null, expectedUser.Email, expectedUser.Profile.Profile, true);
            User actualUser = (User)result.ViewData["User"];

            Assert.That(actualUser.Username == expectedUser.Username
                && actualUser.Email == expectedUser.Email
                && actualUser.Profile.Profile == expectedUser.Profile.Profile);
        }

    }
}

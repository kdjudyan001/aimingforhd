using AimingForHD.Controllers;
using AimingForHD.Entities;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using Moq;
using Microsoft.AspNetCore.Http;
using AimingForHD.Helpers;
using Dapper;

namespace AimingForHDTest.Controllers
{
    public class SearchUserProfileControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Should be able to access page if logged in as admin
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";

            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindProfilesByKeyword());
        }

        //Should redirect if not logged in as admin
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindProfilesByKeyword();

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Should redirect if not logged in as admin
        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["CurrentProfile"] = "3"; //not admin 
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindProfilesByKeyword();

            Assert.IsInstanceOf<RedirectResult>(result);
        }


        //Should return a list of UserProfile
        [Test]
        public void TestFindUserProfiles()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.FindProfilesByKeyword("admin");

            Assert.IsInstanceOf<List<UserProfile>>(result.ViewData["ProfileList"]);
        }

        //Should return a list of UserProfile
        [Test]
        public void TestFindUserProfiles1()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.FindProfilesByKeyword("N");

            Assert.IsTrue((result.ViewData["ProfileList"] as List<UserProfile>).Count > 0);
        }

        [Test]
        public void TestFindUserProfilesNotFound()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            SearchUserProfileController controller = new SearchUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.FindProfilesByKeyword("####");

            Assert.IsTrue((result.ViewData["ProfileList"] as List<UserProfile>).Count == 0);
        }

    }
}
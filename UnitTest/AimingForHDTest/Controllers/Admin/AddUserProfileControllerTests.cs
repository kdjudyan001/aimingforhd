using AimingForHD.Controllers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Moq;
using Microsoft.AspNetCore.Http;
using AimingForHD.Helpers;
using Dapper;

namespace AimingForHDTest.Controllers
{
    public class AddUserProfileControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("DELETE FROM [UserProfile] WHERE Id>4;");
            }
            DbConnection.IsTest(false);
        }

        //Should redirect if not authorized
        [Test]
        public void TestAddByUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewProfile();

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Should redirect if not authorized
        [Test]
        public void TestAddByUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as doctor
            mockSession["CurrentProfile"] = "2"; //not admin
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewProfile();

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Should redirect if not authorized
        [Test]
        public void TestAddByAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewProfile();

            Assert.IsInstanceOf<ViewResult>(result);
        }

        /* Should indicate ViewData["Success"] = false if duplicates are entered
         */
        [Test]
        public void TestAddNewProfile()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.AddNewProfile("Nurse") as ViewResult;

            Assert.AreEqual(true, result.ViewData["Success"]);
        }

        [Test]
        public void TestAddEmptyString()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.AddNewProfile("  ");
            bool success = (bool)result.ViewData["Success"];

            Assert.IsFalse(success);
        }

        /* Should indicate ViewData["Success"] = false if duplicates are entered
         */
        [Test]
        public void TestAddDuplicate()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserProfileController controller = new AddUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.AddNewProfile("Admin") as ViewResult;

            Assert.AreEqual(false, result.ViewData["Success"]);
        }
    }
}
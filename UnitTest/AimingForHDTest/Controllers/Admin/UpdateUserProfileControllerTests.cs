﻿using AimingForHD.Controllers;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace AimingForHDTest.Controllers
{
    class UpdateUserProfileControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE [UserProfile] SET Profile='Doctor' WHERE Profile='Medics';");
            }
            DbConnection.IsTest(false);
        }

        //Display form, if logged in as admin
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserProfileController controller = new UpdateUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.UpdateProfile("Admin1"));
        }

        //Redirect if not authorized as admin
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserProfileController controller = new UpdateUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.UpdateProfile(""));
        }

        [Test]
        public void TestUpdateProfile()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserProfileController controller = new UpdateUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.UpdateProfile("Doctor", "Medics") as ViewResult;

            Assert.AreEqual(true, result.ViewData["Success"]);
        }

        [Test]
        public void TestUpdateProfileEmptyString()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            UpdateUserProfileController controller = new UpdateUserProfileController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.UpdateProfile("Doctor", "  ") as ViewResult;

            Assert.AreEqual(false, result.ViewData["Success"]);
        }

    }
}

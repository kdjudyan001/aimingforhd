using AimingForHD.Controllers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Moq;
using Microsoft.AspNetCore.Http;
using AimingForHD.Helpers;
using Dapper;
using AimingForHD.Entities;

namespace AimingForHDTest.Controllers
{
    public class AddUserControllerTests
    {
        /* Admin1 --> general access
         * Admin2 --> test duplicate/update
         * Admin3 --> test insert
         */

        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            //Restore data
            using(var db = DbConnection.Open())
            {
                db.Query("DELETE FROM [User] WHERE Username = 'Admin3';");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestAddEmptyString()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            ViewResult result = (ViewResult)controller.AddNewUser("","","","");

            Assert.AreNotEqual(true, result.ViewData["Success"]);
        }

        //Should redirect if not authorized
        [Test]
        public void TestAddByUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewUser("","","","");

            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Should redirect if not authorized
        [Test]
        public void TestAddByAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.AddNewUser("","","","");

            Assert.IsInstanceOf<ViewResult>(result);
        }

        /* Should return ViewData["Success"] = false if duplicates are entered
         */
        [Test]
        public void TestAddDuplicate()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            var result = controller.AddNewUser("Admin2","Admin123","Admin","admin1@gmail.com") as ViewResult;

            Assert.AreEqual(false, result.ViewData["Success"]);
        }

        [Test]
        public void TestAdd()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var newUser = new User()
            {
                Username = "Admin3",
                Password = "12345678",
                Profile = new UserProfile() { Profile = "Admin" },
                Email = "admin3@gmail.com"
            };

            var result = (ViewResult)controller.AddNewUser(newUser.Username, newUser.Password,
                newUser.Profile.Profile, newUser.Email);

            Assert.AreEqual(true, result.ViewData["Success"]);
        }

        [Test]
        public void TestAddInvalidUsername()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var newUser = new User()
            {
                Username = "  ",
                Password = "1237a",
                Profile = new UserProfile() { Profile = "Admin" },
                Email = "admin3@gmail.com"
            };

            var result = (ViewResult)controller.AddNewUser(newUser.Username, newUser.Password,
                newUser.Profile.Profile, newUser.Email);

            Assert.AreNotEqual(true, result.ViewData["Success"]);
        }

        [Test]
        public void TestAddInvalidPassword()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var newUser = new User()
            {
                Username = "Admin3",
                Password = "123",
                Profile = new UserProfile() { Profile = "Admin" },
                Email = "admin3@gmail.com"
            };

            var result = (ViewResult)controller.AddNewUser(newUser.Username, newUser.Password,
                newUser.Profile.Profile, newUser.Email);

            Assert.AreNotEqual(true, result.ViewData["Success"]);
        }
        

        [Test]
        public void TestAddInvalidEmail()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();

            //Set login as admin
            mockSession["CurrentProfile"] = "1";
            mockSession["IsLoggedIn"] = "true";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            AddUserController controller = new AddUserController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var newUser = new User()
            {
                Username = "Admin3",
                Password = "1234567a",
                Profile = new UserProfile() { Profile = "Admin" },
                Email = "admin3mail"
            };

            var result = (ViewResult)controller.AddNewUser(newUser.Username, newUser.Password,
                newUser.Profile.Profile, newUser.Email);

            Assert.AreNotEqual(true, result.ViewData["Success"]);
        }
    }
}
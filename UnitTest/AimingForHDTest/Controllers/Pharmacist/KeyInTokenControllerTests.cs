﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class KeyInTokenControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as pharmacist
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindPrescriptionByToken());
        }

        //Display list of users if logged in as pharmacist
        [Test]
        public void TestAuthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindPrescriptionByToken(100000));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindPrescriptionByToken();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1"; //not a pharmacist
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("GET");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindPrescriptionByToken();
            Assert.IsInstanceOf<RedirectResult>(result);
        }


        //Redirect if not authorized
        [Test]
        public void TestUnauthorized3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1"; //not a pharmacist
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindPrescriptionByToken();
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "1"; //not a pharmacist
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindPrescriptionByToken(100000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestPrescriptionToken()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescriptionByToken(100000);
            Assert.IsInstanceOf<Prescription>(result.ViewData["Prescription"]);
        }

        [Test]
        public void TestPrescriptionToken2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescriptionByToken(100001);
            Assert.IsInstanceOf<Prescription>(result.ViewData["Prescription"]);
        }

        [Test]
        public void TestPrescriptionToken3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            int token = 100001;
            var result = (ViewResult)controller.FindPrescriptionByToken(token);
            Assert.AreEqual(token, result.ViewData["token"]);
        }

        /* ViewData["Prescription"] should be null if prescription not found
         */
        [Test]
        public void TestPrescriptionToken4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            KeyInTokenController controller = new KeyInTokenController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            int token = -1; //does not exist
            var result = (ViewResult)controller.FindPrescriptionByToken(token);
            Assert.IsNull(result.ViewData["Prescription"]);
        }
    }
}

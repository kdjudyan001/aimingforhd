﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Controllers
{
    class PharmacistSearchPrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            DbConnection.IsTest(false);
        }

        //Display list of users if logged in as doctor
        [Test]
        public void TestAuthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.FindPrescByKeyword("100000"));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorized()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.FindPrescByKeyword("100000");
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestFindPrescription()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescByKeyword("100000");
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescByKeyword("doctor1");
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        [Test]
        public void TestFindPrescription3()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescByKeyword("Patient1");
            Assert.IsInstanceOf<List<Prescription>>(result.ViewData["PrescriptionList"]);
        }

        /* Check that the List<Medicine> are correctly assigned
         */
        [Test]
        public void TestFindPrescription4()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistSearchPrescriptionController controller = new PharmacistSearchPrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = (ViewResult)controller.FindPrescByKeyword("Patient1");
            List<Prescription> prescriptions = (List<Prescription>)result.ViewData["PrescriptionList"];

            Assert.That(()=> prescriptions.TrueForAll(
                    p => p.MedicineList.TrueForAll(m => m.PrescriptionId == p.PrescriptionId)
                )
            );
        }
    }
}

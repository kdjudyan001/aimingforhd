﻿using AimingForHD.Controllers;
using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace AimingForHDTest.Controllers
{
    class PharmacistUpdatePrescriptionControllerTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true); //Connect to test DB
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE Medicine SET Name='Med2', Amount=2 WHERE MedicineId=1001");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestAuthorizedRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveMed(1000));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorizedRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.RetrieveMed(1000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorizedRetrieveMed2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "3"; //not pharmacist
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            var result = controller.RetrieveMed(1000);
            Assert.IsInstanceOf<RedirectResult>(result);
        }

        [Test]
        public void TestRetrieveMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<Medicine>((controller.RetrieveMed(1000) as ViewResult).ViewData["Medicine"]);
        }

        [Test]
        public void TestRetrieveMedNotFound()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.RetrieveMed(10));
        }

        [Test]
        public void TestRetrieveMedNotFound2()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsNull((controller.RetrieveMed(10) as ViewResult).ViewData["Medicine"]);
        }

        [Test]
        public void TestAuthorizedDispenseMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<ViewResult>(controller.DispenseMed(1001,10));
        }

        //Redirect if not authorized
        [Test]
        public void TestUnauthorizedDispenseMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);
            mockHttpContext.Setup(s => s.Request.Method).Returns("POST");

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Assert.IsInstanceOf<RedirectResult>(controller.DispenseMed(1001,10));
        }


        [Test]
        public void TestDispenseMed()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Medicine expected = new()
            {
                MedicineId = 1001,
                Amount = 1
            };

            Medicine actual = (Medicine)(controller.DispenseMed(1001, 1) as ViewResult).ViewData["Medicine"];

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Amount == actual.Amount
            );
        }

        //Try to dispense more than current amount
        [Test]
        public void TestDispenseMedFail()
        {
            //Mock Http Session
            Mock<HttpContext> mockHttpContext = new Mock<HttpContext>();
            MockHttpSession mockSession = new MockHttpSession();
            mockSession["IsLoggedIn"] = "true";
            mockSession["CurrentProfile"] = "4";
            mockHttpContext.Setup(s => s.Session).Returns(mockSession);

            PharmacistUpdatePrescriptionController controller = new PharmacistUpdatePrescriptionController();
            controller.ControllerContext.HttpContext = mockHttpContext.Object;

            Medicine expected = new()
            {
                MedicineId = 1001,
                Amount = 2
            };

            Medicine actual = (Medicine)(controller.DispenseMed(1001, 99) as ViewResult).ViewData["Medicine"];

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Amount == actual.Amount
            );
        }

    }
}

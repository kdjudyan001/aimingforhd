﻿using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Entities
{
    class UserTests
    {
        /* Admin1 --> general access
         * Admin2 --> test duplicate & update (try to update to 'Admin20')
         * Admin3 --> test insert
         */

        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true); //Connect to test DB
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("DELETE FROM [User] WHERE Username='Admin3';");
                db.Query("UPDATE [User] SET Username='Admin2', " +
                    "Password=HASHBYTES('SHA2_512',CONVERT(VARCHAR(50),'Admin123')), " +
                    "Email='admin2@fakemail.com', Profile=1 WHERE Username='Admin20';");
                db.Query("UPDATE [User] SET Username='Patient2', " +
                    "Password=HASHBYTES('SHA2_512',CONVERT(VARCHAR(50),'Patient123')), " +
                    "Email='patient2@fakemail.com', Profile=3 WHERE Username='Patient20';");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestFindUser()
        {
            Assert.That(User.FindUser("Admin1", "Admin123") is User);
        }

        [Test]
        public void TestFindUser2()
        {
            Assert.That(User.FindUser("Admin2", "Admin123") is User);
        }

        [Test]
        public void TestFindUser3()
        {
            Assert.That(User.FindUser("Doctor1", "Doctor123") is User);
        }

        [Test]
        public void TestFindUser4()
        {
            Assert.That(User.FindUser("Patient1", "Patient123") is User);
        }

        //Test that function return null if user is not found
        [Test]
        public void TestFindUserNotFound()
        {
            Assert.IsNull(User.FindUser("", ""));
        }

        [Test]
        public void TestFindUserNotFound2()
        {
            Assert.IsNull(User.FindUser("Admin", "Adminnnn")); //wrong password
        }

        //Test that insertion for duplicate entry will fail
        [Test]
        public void TestInsertDuplicate()
        {
            Assert.IsFalse(User.AddNewUser("Admin2","","Admin", "Admin2@fakemail.com"));
        }

        [Test]
        public void TestGetAllUsers()
        {
            Assert.That(User.GetAllUsers() is List<User>);
        }

        [Test]
        public void TestGetAllUsers2()
        {
            Assert.That(User.GetAllUsers().Count>0);
        }

        [Test]
        public void TestGetUser()
        {
            Assert.That(User.GetUser("Admin1") is User);
        }

        [Test]
        public void TestGetUser2()
        {
            Assert.That(User.GetUser("Doctor1") is User);
        }

        [Test]
        public void TestGetUser3()
        {
            Assert.That(User.GetUser("Patient1") is User);
        }

        [Test]
        public void TestUpdateUserInfoSuccess()
        {
            User expectedUser = new User()
            {
                Username = "Admin20",
                Email = "adminnewmail@fakemail.com",
                Profile = new UserProfile() { Profile = "Admin" },
            };

            User actualUser = User.UpdateUserInfo("Admin2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, expectedUser.Profile.Profile, false);

            Assert.That(actualUser.Username == expectedUser.Username &&
                actualUser.Email == expectedUser.Email &&
                actualUser.Profile.Profile == expectedUser.Profile.Profile);
        }

        [Test]
        public void TestUpdateUserInfoSuccess2()
        {
            User expectedUser = new User()
            {
                Username = "Admin20",
                Email = "adminnewmail@fakemail.com",
                Profile = new UserProfile() { Profile = "Patient" },
            };

            User actualUser = User.UpdateUserInfo("Admin2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, expectedUser.Profile.Profile, false);

            Assert.That(actualUser.Username == expectedUser.Username &&
                actualUser.Email == expectedUser.Email &&
                actualUser.Profile.Profile == expectedUser.Profile.Profile);
        }

        [Test]
        public void TestUpdatePatientInfoSuccess3()
        {
            User expectedUser = new User()
            {
                Username = "Patient20",
                Password = "",
                Email = "patientnewmail@fakemail.com"
            };

            User actualUser = User.UpdateUserInfo("Patient2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, false);

            Assert.That(actualUser.Username == expectedUser.Username &&
                actualUser.Email == expectedUser.Email);
        }

        [Test]
        public void TestUpdateUserInfoFail()
        {
            User expectedUser = new User()
            {
                Username = "Doctor1", //existing username
                Email = "docmail@fakemail.com",
                Profile = new UserProfile() { Profile = "Doctor" },
            };

            User actualUser = User.UpdateUserInfo("Admin2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, expectedUser.Profile.Profile, false);

            Assert.IsNull(actualUser);
        }

        [Test]
        public void TestUpdatePatientInfo2()
        {
            User expectedUser = new User()
            {
                Username = "Patient20",
                Password = "12345newpw",
                Email = "patientnewmail@fakemail.com",
            };

            User actualUser = User.UpdateUserInfo("Patient2", expectedUser.Username,
                expectedUser.Password, expectedUser.Email, true);

            Assert.That(actualUser.Username == expectedUser.Username &&
                actualUser.Email == expectedUser.Email &&
                actualUser.Profile.Profile == expectedUser.Profile.Profile);
        }

        [Test]
        public void TestAddUser()
        {
            Assert.IsTrue(User.AddNewUser("Admin3", "abcde123", "Admin", "admin11@fakemail.com"));
        }

        [Test]
        public void TestAddDuplicateUser()
        {
            Assert.IsFalse(User.AddNewUser("Admin1", "Admin123", "Admin", "admin11@fakemail.com"));
        }

        [Test]
        public void TestAddDuplicateUser2()
        {
            Assert.IsFalse(User.AddNewUser("Doctor1", "Admin123", "Admin", "admin11@fakemail.com"));
        }

        [Test]
        public void TestAddDuplicateUser3()
        {
            Assert.IsFalse(User.AddNewUser("Patient1", "Admin123", "Admin", "admin11@fakemail.com"));
        }

        [Test]
        public void TestAddWithNonExistentProfile()
        {
            //User Profile "User" does not exist in test database
            Assert.IsFalse(User.AddNewUser("Admin3", "abcde123", "User", "admin11@fakemail.com"));
        }
    }
}

﻿using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using Microsoft.Data.SqlClient;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Entities
{
    class UserProfileTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true);
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("DELETE FROM [UserProfile] WHERE Id>4;");
                db.Query("UPDATE [UserProfile] SET Profile='Doctor' WHERE Profile='Medics';");
            }
            DbConnection.IsTest(false);
        }

        //Test that function returns a List<UserProfile>
        [Test]
        public void TestGetListOfUserProfiles()
        {
            Assert.That(UserProfile.GetUserProfiles() is List<UserProfile>);
        }

        //Test that duplicate entry insert will fail
        [Test]
        public void TestInsertDuplicate()
        {
            Assert.IsFalse(UserProfile.AddNewProfile("Admin"));
        }

        [Test]
        public void TestUpdate()
        {
            Assert.IsTrue(UserProfile.UpdateUserProfile("Doctor", "Medics"));
        }

        //Should not allow if new value already exists
        [Test]
        public void TestUpdateWithDuplicate()
        {
            Assert.IsFalse(UserProfile.UpdateUserProfile("Doctor", "Patient"));
        }

        //Should not allow if new value already exists
        [Test]
        public void TestUpdateWithDuplicate2()
        {
            Assert.IsFalse(UserProfile.UpdateUserProfile("Doctor", "Admin"));
        }
    }
}

﻿using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Entities
{
    class MedicineTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true); //Connect to test DB
        }

        [TearDown]
        public void Teardown()
        {
            using (var db = DbConnection.Open())
            {
                db.Query("UPDATE Medicine SET Name='Med2', Amount=2 WHERE MedicineId=1001");
                db.Query("DELETE FROM Medicine WHERE MedicineId>1003");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestGetMedicineByPrescription()
        {
            Assert.IsTrue(Medicine.GetMedicinesByPrescription(100000).Count > 0);
        }

        [Test]
        public void TestGetMedicineByPrescription2()
        {
            Assert.That(Medicine.GetMedicinesByPrescription(10).Count==0); //non-existent prescriptionId
        }

        [Test]
        public void TestAddNewMedicine()
        {
            Assert.That(Medicine.AddNewMedicine("Med5", 3, 100000) is Medicine);
        }

        [Test]
        public void TestAddNewMedicine2()
        {
            Assert.IsNull(Medicine.AddNewMedicine("Med5", 3, 100)); //non-existent prescriptionId
        }

        [Test]
        public void TestAddNewMedicine3()
        {
            Assert.IsNull(Medicine.AddNewMedicine("Med5", -3, 100000)); //amount cannot be negative
        }


        [Test]
        public void TestGetMedById()
        {
            Assert.IsNotNull(Medicine.GetMed(1000));
        }

        [Test]
        public void TestGetMedById2()
        {
            //Id does not exist
            Assert.IsNull(Medicine.GetMed(-1));
        }

        [Test]
        public void TestGetMedById3()
        {
            //Id does not exist
            Assert.IsNull(Medicine.GetMed(100));
        }

        [Test]
        public void TestUpdateMed()
        {
            Medicine expected = new()
            {
                MedicineId = 1001,
                Name = "Med20",
                Amount = 10,
                PrescriptionId = 100000
            };

            Medicine actual = Medicine.UpdateMed(expected.MedicineId, expected.Name, expected.Amount);

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Name == actual.Name &&
                expected.Amount == actual.Amount &&
                expected.PrescriptionId == actual.PrescriptionId
            );
        }

        [Test]
        public void TestUpdateMedFail()
        {
            //This medicine id does not exist
            Medicine expected = new()
            {
                MedicineId = 11,
                Name = "Med20",
                Amount = 10
            };

            Medicine actual = Medicine.UpdateMed(expected.MedicineId, expected.Name, expected.Amount);

            Assert.IsNull(actual);
        }

        [Test]
        public void TestUpdateMedFail2()
        {
            //Amount cannot be negative
            Medicine expected = new()
            {
                MedicineId = 1001,
                Name = "Med20",
                Amount = -10
            };

            Medicine actual = Medicine.UpdateMed(expected.MedicineId, expected.Name, expected.Amount);

            Assert.IsNull(actual);
        }

        [Test]
        public void TestDispenseMed()
        {
            Medicine expected = new()
            {
                MedicineId = 1001,
                Name = "Med2",
                Amount = 0,
                PrescriptionId = 100000
            };

            Medicine actual = Medicine.DispenseMed(expected.MedicineId, 2);

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Name == actual.Name &&
                expected.Amount == actual.Amount &&
                expected.PrescriptionId == actual.PrescriptionId
            );
        }

        [Test]
        public void TestDispenseMedFail()
        {
            Medicine expected = new()
            {
                MedicineId = 1001,
                Name = "Med2",
                Amount = 2,
                PrescriptionId = 100000
            };

            Medicine actual = Medicine.DispenseMed(expected.MedicineId, 100); //cannot dispense more than current amount

            Assert.That(
                expected.MedicineId == actual.MedicineId &&
                expected.Name == actual.Name &&
                expected.Amount == actual.Amount &&
                expected.PrescriptionId == actual.PrescriptionId
            );
        }

        [Test]
        public void TestDispenseMedFail2()
        {
            Medicine expected = new()
            {
                MedicineId = -1, //does not exist
                Name = "Med2",
                Amount = 5
            };

            Medicine actual = Medicine.DispenseMed(expected.MedicineId, expected.Amount);

            Assert.IsNull(actual);
        }
    }
}

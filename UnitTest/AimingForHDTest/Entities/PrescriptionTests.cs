﻿using AimingForHD.Entities;
using AimingForHD.Helpers;
using Dapper;
using NUnit.Framework;
using System.Collections.Generic;

namespace AimingForHDTest.Entities
{
    class PrescriptionTests
    {
        [SetUp]
        public void Setup()
        {
            DbConnection.IsTest(true); //Connect to test DB
        }

        [TearDown]
        public void Teardown()
        {
            using(var db = DbConnection.Open())
            {
                db.Query("DELETE FROM [Prescription] WHERE PrescriptionId>100003;");
            }
            DbConnection.IsTest(false);
        }

        [Test]
        public void TestGetPrescription()
        {
            Assert.That(Prescription.GetPrescriptionByToken(100000) is Prescription);
        }

        [Test]
        public void TestGetPrescription2()
        {
            Assert.That(Prescription.GetPrescriptionByToken(100001) is Prescription);
        }

        [Test]
        public void TestGetPrescription3()
        {
            Assert.That(Prescription.GetPrescriptionByToken(-1) is null);
        }

        [Test]
        public void TestGetAllPrescriptions()
        {
            Assert.IsInstanceOf<List<Prescription>>(Prescription.GetPrescriptions());
        }

        [Test]
        public void TestGetAllPrescriptions2()
        {
            Assert.IsTrue(Prescription.GetPrescriptions().Count>0);
        }

        [Test]
        public void TestAddPrescription()
        {
            Assert.IsNotNull(Prescription.AddNewPrescription("Doctor1", "Patient1"));
        }

        //Should not insert and return null if the first parameter is not a doctor username
        //Should not insert and return null if the second parameter is not a patient username
        [Test]
        public void TestAddPrescriptionInvalid()
        {
            Assert.IsNull(Prescription.AddNewPrescription("Admin1", "Admin2"));
        }

        //Should not insert and return null if the first parameter is not a doctor username
        [Test]
        public void TestAddPrescriptionInvalidDoctor()
        {
            Assert.IsNull(Prescription.AddNewPrescription("Admin1", "Patient1"));
        }

        //Should not insert and return null if the second parameter is not a patient username
        [Test]
        public void TestAddPrescriptionInvalidPatient()
        {
            Assert.IsNull(Prescription.AddNewPrescription("Doctor1", "Admin1"));
        }

        [Test]
        public void TestGetPrescriptions()
        {
            Assert.IsNotNull(Prescription.GetPrescriptions(104));
        }

        [Test]
        public void TestGetPrescriptions2()
        {
            Assert.IsNull(Prescription.GetPrescriptions(1)); //Non-existent user
        }

        [Test]
        public void TestGetPrescriptions3()
        {
            Assert.IsNull(Prescription.GetPrescriptions(100)); //Non-patient user
        }

        [Test]
        public void TestGetPrescriptionsByKeyword()
        {
            Assert.That(Prescription.GetPrescriptionsByKeyword("doc").Count > 0);
        }

        [Test]
        public void TestGetPrescriptionsByKeyword2()
        {
            Assert.That(Prescription.GetPrescriptionsByKeyword("100000").Count > 0);
        }

        [Test]
        public void TestGetPrescriptionsByKeyword3()
        {
            Assert.That(Prescription.GetPrescriptionsByKeyword("patient1").Count > 0);
        }

        [Test]
        public void TestGetPrescriptionsByKeyword4()
        {
            Assert.IsNull(Prescription.GetPrescriptionsByKeyword("med"));
        }

        [Test]
        public void TestSearchPatientPrescription()
        {
            Assert.IsTrue(Prescription.GetMyPrescriptions("100", 104).Count > 0);
        }

        [Test]
        public void TestSearchPatientPrescription2()
        {
            Assert.That(Prescription.GetMyPrescriptions("100", 104) is List<Prescription>);
        }

        [Test]
        public void TestSearchPatientPrescription3()
        {
            Assert.IsNull(Prescription.GetMyPrescriptions("100", 100)); //User ID 100 is not a patient
        }

        [Test]
        public void TestSearchPatientPrescription4()
        {
            Assert.That(Prescription.GetMyPrescriptions("doctor1", 104) is List<Prescription>);
        }

        [Test]
        public void TestSearchPatientPrescription5()
        {
            //Assert that all prescriptions returned belong to the specified patient
            Assert.IsTrue(
                Prescription.GetMyPrescriptions("100", 104).TrueForAll(p => p.Patient.Id == 104)
            );
        }
    }
}

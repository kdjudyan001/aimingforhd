﻿using System.Collections.Generic;
using System.Linq;
using AimingForHD.Helpers;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Data.SqlClient;

namespace AimingForHD.Entities
{
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        public int Id { get; set; }

        public string Profile { get; set; }

        /* Return a list of all user profiles
         */
        public static List<UserProfile> GetUserProfiles()
        {
            List<UserProfile> profileList = new();

            try
            { 
                using (var db = DbConnection.Open())
                {
                    profileList = db.GetAll<UserProfile>().ToList();
                }
            }
            catch { }

            return profileList;
        }

        /* Insert new UserProfile to DB
         */
        public static bool AddNewProfile(string userProfile)
        {
            try
            {
                using (var db = DbConnection.Open())
                {
                string sql = "INSERT INTO [UserProfile] ([Profile]) VALUES (@profile);";

                    db.Query(sql, new { profile = userProfile });
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        /* Return a list of UserProfile that contains the keyword
         * Case-insensitive match
         */
        public static List<UserProfile> GetProfilesByKeyword(string keyword)
        {
            List<UserProfile> profileList = new List<UserProfile>();
            try 
            { 
                using(var db = DbConnection.Open())
                {
                    string sql = "SELECT Id, Profile FROM [UserProfile] " +
                        "WHERE Profile COLLATE SQL_Latin1_General_CP1_CI_AS LIKE @keyword;";
                    profileList = db.Query<UserProfile>(sql, new { keyword = "%" + keyword + "%" }).ToList();
                }
            }
            catch { }
            return profileList;
        }

        public static bool UpdateUserProfile(string profileOld, string profileNew)
        {
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sqlUpdate = "UPDATE [UserProfile] SET Profile=@profileNew WHERE Profile=@profileOld;";

                    //Check if new value already exists
                    string sqlCheck = "SELECT Id, Profile FROM [UserProfile] WHERE Profile=@profileNew";
                    var checkProfile = db.Query(sqlCheck, new { profileNew });

                    if (checkProfile.Any())
                    {
                        return false;
                    }
                    else
                    {
                        db.Query(sqlUpdate, new { profileNew = profileNew, profileOld = profileOld });
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }

}

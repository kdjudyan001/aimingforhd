﻿using AimingForHD.Helpers;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace AimingForHD.Entities
{
    [Table("Prescription")]
    public class Prescription
    {
        public int PrescriptionId { get; set; }
        public User Doctor { get; set; }
        public User Patient { get; set; }
        public List<Medicine> MedicineList { get; set; }

        public static Prescription GetPrescriptionByToken(int id)
        {
            Prescription p;
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT PrescriptionId, Doctor.Id as DoctorId, " +
                        "Doctor.Username AS DoctorUsername, Doctor.Email AS DoctorEmail, " +
                        "DoctorProfile.Profile AS DoctorProfile, Patient.Id AS PatientId, " +
                        "Patient.Username AS PatientUsername, Patient.Email AS PatientEmail, " +
                        "PatientProfile.Profile AS PatientProfile FROM [Prescription] " +
                        "JOIN [User] AS Doctor ON [Prescription].DoctorId = Doctor.Id " +
                        "JOIN [User] AS Patient ON PatientId = Patient.Id " +
                        "JOIN [UserProfile] AS DoctorProfile ON Doctor.Profile = DoctorProfile.Id " +
                        "JOIN [UserProfile] AS PatientProfile ON Patient.Profile = PatientProfile.Id " +
                        "WHERE PrescriptionId=@id";

                    var result = db.QuerySingleOrDefault(sql, new { id });

                    p = new Prescription()
                    {
                        PrescriptionId = result.PrescriptionId,
                        Doctor = new User()
                        {
                            Id = result.DoctorId,
                            Username = result.DoctorUsername,
                            Email = result.DoctorEmail,
                            Profile = new() { Profile = result.DoctorProfile }
                        },
                        Patient = new User()
                        {
                            Id = result.PatientId,
                            Username = result.PatientUsername,
                            Email = result.PatientEmail,
                            Profile = new() { Profile = result.PatientProfile }
                        }
                    };
                }
            }
            catch { return null; }

            return p;
        }

        public static List<Prescription> GetPrescriptions()
        {
            List<Prescription> prescriptions = new();
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT PrescriptionId, Doctor.Id as DoctorId, " +
                        "Doctor.Username AS DoctorUsername, Doctor.Email AS DoctorEmail, " +
                        "DoctorProfile.Profile AS DoctorProfile, Patient.Id AS PatientId, " +
                        "Patient.Username AS PatientUsername, Patient.Email AS PatientEmail, " +
                        "PatientProfile.Profile AS PatientProfile FROM [Prescription] " +
                        "JOIN [User] AS Doctor ON [Prescription].DoctorId = Doctor.Id " +
                        "JOIN [User] AS Patient ON PatientId = Patient.Id " +
                        "JOIN [UserProfile] AS DoctorProfile ON Doctor.Profile = DoctorProfile.Id " +
                        "JOIN [UserProfile] AS PatientProfile ON Patient.Profile = PatientProfile.Id ";

                    var result = db.Query(sql);

                    foreach(var row in result)
                    { 
                        Prescription p = new Prescription()
                        {
                            PrescriptionId = row.PrescriptionId,
                            Doctor = new User()
                            {
                                Id = row.DoctorId,
                                Username = row.DoctorUsername,
                                Email = row.DoctorEmail,
                                Profile = new() { Profile = row.DoctorProfile }
                            },
                            Patient = new User()
                            {
                                Id = row.PatientId,
                                Username = row.PatientUsername,
                                Email = row.PatientEmail,
                                Profile = new() { Profile = row.PatientProfile }
                            }
                        };
                        prescriptions.Add(p);
                    }
                }
            }
            catch { return null; }

            return prescriptions;
        }

        public static Prescription AddNewPrescription(string doctor, string patient)
        {
            Prescription newPrescription = null;

            try 
            { 
                using(var db = DbConnection.Open())
                {
                    string doctorQuery = "SELECT U.Id FROM [User] AS U JOIN [Userprofile] AS P ON U.Profile = P.Id WHERE Username=@doctor AND P.Id=2";
                    var doctorUser = db.QuerySingle(doctorQuery, new { doctor });
                    string patientQuery = "SELECT U.Id FROM [User] AS U JOIN [Userprofile] AS P ON U.Profile = P.Id WHERE Username=@patient AND P.Id=3";
                    var patientUser = db.QuerySingle(patientQuery, new { patient });
                    string sql = "INSERT INTO Prescription (DoctorId, PatientId) OUTPUT INSERTED.PrescriptionId VALUES (@doctorId, @patientId)";

                    var insert = db.QuerySingle(sql, new { doctorId = doctorUser.Id, patientId = patientUser.Id });
                    newPrescription = GetPrescriptionByToken(insert.PrescriptionId);
                }
            }
            catch
            {
                return null;
            }

            return newPrescription;
        }

        public static List<Prescription> GetPrescriptionsByKeyword(string keyword)
        {
            List<Prescription> prescriptions = new();
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT PrescriptionId, Doctor.Id as DoctorId, " +
                        "Doctor.Username AS DoctorUsername, Doctor.Email AS DoctorEmail, " +
                        "DoctorProfile.Profile AS DoctorProfile, Patient.Id AS PatientId, " +
                        "Patient.Username AS PatientUsername, Patient.Email AS PatientEmail, " +
                        "PatientProfile.Profile AS PatientProfile FROM [Prescription] " +
                        "JOIN [User] AS Doctor ON [Prescription].DoctorId = Doctor.Id " +
                        "JOIN [User] AS Patient ON PatientId = Patient.Id " +
                        "JOIN [UserProfile] AS DoctorProfile ON Doctor.Profile = DoctorProfile.Id " +
                        "JOIN [UserProfile] AS PatientProfile ON Patient.Profile = PatientProfile.Id ";

                    var result = db.Query(sql);

                    foreach (var row in result)
                    {
                        Prescription p = new Prescription()
                        {
                            PrescriptionId = row.PrescriptionId,
                            Doctor = new User()
                            {
                                Id = row.DoctorId,
                                Username = row.DoctorUsername,
                                Email = row.DoctorEmail,
                                Profile = new() { Profile = row.DoctorProfile }
                            },
                            Patient = new User()
                            {
                                Id = row.PatientId,
                                Username = row.PatientUsername,
                                Email = row.PatientEmail,
                                Profile = new() { Profile = row.PatientProfile }
                            }
                        };
                        prescriptions.Add(p);
                    }
                }

                prescriptions = prescriptions.FindAll(p => (
                    p.Doctor.Username.ToUpper().Contains(keyword.ToUpper()) ||
                    p.Patient.Username.ToUpper().Contains(keyword.ToUpper()) ||
                    p.PrescriptionId.ToString().Contains(keyword)
                ));

            }
            catch { return null; }

            return prescriptions.Count == 0 ? null : prescriptions;
        }

        public static List<Prescription> GetPrescriptions(int patientId)
        {
            List<Prescription> prescriptions = new();
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT PrescriptionId, Doctor.Id as DoctorId, " +
                        "Doctor.Username AS DoctorUsername, Doctor.Email AS DoctorEmail, " +
                        "DoctorProfile.Profile AS DoctorProfile, Patient.Id AS PatientId, " +
                        "Patient.Username AS PatientUsername, Patient.Email AS PatientEmail, " +
                        "PatientProfile.Profile AS PatientProfile FROM [Prescription] " +
                        "JOIN [User] AS Doctor ON [Prescription].DoctorId = Doctor.Id " +
                        "JOIN [User] AS Patient ON PatientId = Patient.Id " +
                        "JOIN [UserProfile] AS DoctorProfile ON Doctor.Profile = DoctorProfile.Id " +
                        "JOIN [UserProfile] AS PatientProfile ON Patient.Profile = PatientProfile.Id " +
                        "WHERE PatientId=@patientId";
                    var result = db.Query(sql, new { patientId });
                    
                    foreach (var row in result)
                    {
                        Prescription p = new Prescription()
                        {
                            PrescriptionId = row.PrescriptionId,
                            Doctor = new User()
                            {
                                Id = row.DoctorId,
                                Username = row.DoctorUsername,
                                Email = row.DoctorEmail,
                                Profile = new() { Profile = row.DoctorProfile }
                            },
                            Patient = new User()
                            {
                                Id = row.PatientId,
                                Username = row.PatientUsername,
                                Email = row.PatientEmail,
                                Profile = new() { Profile = row.PatientProfile }
                            }
                        };
                        prescriptions.Add(p);
                    }
                }
            }
            catch { return null; }

            return prescriptions.Count==0 ? null : prescriptions;
        }

        public static List<Prescription> GetMyPrescriptions(string keyword, int myId)
        {
            List<Prescription> prescriptions = new();
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT PrescriptionId, Doctor.Id as DoctorId, " +
                        "Doctor.Username AS DoctorUsername, Doctor.Email AS DoctorEmail, " +
                        "DoctorProfile.Profile AS DoctorProfile, Patient.Id AS PatientId, " +
                        "Patient.Username AS PatientUsername, Patient.Email AS PatientEmail, " +
                        "PatientProfile.Profile AS PatientProfile FROM [Prescription] " +
                        "JOIN [User] AS Doctor ON [Prescription].DoctorId = Doctor.Id " +
                        "JOIN [User] AS Patient ON PatientId = Patient.Id " +
                        "JOIN [UserProfile] AS DoctorProfile ON Doctor.Profile = DoctorProfile.Id " +
                        "JOIN [UserProfile] AS PatientProfile ON Patient.Profile = PatientProfile.Id " +
                        "WHERE PatientId=@patientId";

                    var result = db.Query(sql, new { patientId = myId });

                    foreach (var row in result)
                    {
                        Prescription p = new Prescription()
                        {
                            PrescriptionId = row.PrescriptionId,
                            Doctor = new User()
                            {
                                Id = row.DoctorId,
                                Username = row.DoctorUsername,
                                Email = row.DoctorEmail,
                                Profile = new() { Profile = row.DoctorProfile }
                            },
                            Patient = new User()
                            {
                                Id = row.PatientId,
                                Username = row.PatientUsername,
                                Email = row.PatientEmail,
                                Profile = new() { Profile = row.PatientProfile }
                            }
                        };
                        prescriptions.Add(p);
                    }
                }
                prescriptions = prescriptions.FindAll(p => (
                p.Doctor.Username.ToUpper().Contains(keyword.ToUpper()) ||
                p.Patient.Username.ToUpper().Contains(keyword.ToUpper()) ||
                p.PrescriptionId.ToString().Contains(keyword)
            ));

            }
            catch { return null; }

            return prescriptions.Count == 0 ? null : prescriptions;
        }

    }
}

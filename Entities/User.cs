﻿
using AimingForHD.Helpers;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace AimingForHD.Entities
{
    [Table("User")]
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get;  set; }

        public string Email { get; set; }

        public UserProfile Profile { get; set; }

        public User()
        {
            Profile = new UserProfile();
        }

        /* Return a User with the corresponding username and password
         * Will return null if no match found in DB
         */
        public static User FindUser(string username, string password)
        {
            User user = null;

            try
            {
                using (var db = DbConnection.Open())
                {
                    //SQL query
                    string query = "SELECT [User].Id AS Id, Email, [UserProfile].Profile AS Profile, [UserProfile].Id AS ProfileId" +
                        " FROM [User],[UserProfile] WHERE Username=@username" +
                        " AND Password=( HASHBYTES('SHA2_512', CONVERT(VARCHAR(50),@password) ) ) " +
                        " AND [UserProfile].Id = [User].Profile;";

                    //Get result from DB
                    var result = db.QuerySingleOrDefault(query, new { username = username, password = password });

                    if (result != null)
                    {
                        user = new User()
                        {
                            Id= result.Id,
                            Username = username,
                            Password = password,
                            Email = result.Email,
                            Profile = new UserProfile() { Id = result.ProfileId, Profile = result.Profile }
                        };
                    }
                }
            }
            catch { }

            return user;
        }

        /* Return a User based on username
         * Will return null if no match found in DB
         */
        public static User GetUser(string username)
        {
            User user = null;

            try
            {
                using (var db = DbConnection.Open())
                {
                    //SQL query
                    string query = "SELECT [User].Id AS Id, Username, Email, [UserProfile].Profile AS Profile, [UserProfile].Id AS ProfileId" +
                        " FROM [User], [UserProfile] WHERE Username=@username AND [User].Profile = [UserProfile].Id;";

                    //Get result from DB
                    var result = db.QuerySingleOrDefault(query, new { username = username });

                    if (result != null)
                    {
                        UserProfile tempProfile = new UserProfile() { Id = result.ProfileId, Profile = result.Profile };
                        user = new User()
                        {
                            Id = result.Id,
                            Username = result.Username,
                            Email = result.Email,
                            Profile = tempProfile
                        };
                    }
                }
            }
            catch { }

            return user;
        }

        /* Return a list of all users in the DB
         */
        public static List<User> GetAllUsers()
        {
            List<User> userList = new();

            try
            {
                using (var db = DbConnection.Open())
                {
                    string query = "SELECT [User].Id AS Id, Username, Email, [UserProfile].Profile AS Profile, [UserProfile].Id AS ProfileId" +
                        " FROM [User], [UserProfile] WHERE [User].Profile = [UserProfile].Id;";

                    var result = db.Query(query);
                    foreach (var row in result)
                    {
                        User user = new User()
                        {
                            Id = row.Id,
                            Username = row.Username,
                            Email = row.Email,
                            Profile = new UserProfile() { Id = row.ProfileId, Profile = row.Profile }
                        };
                        userList.Add(user);
                    }
                }
            }
            catch { }

            return userList;
        }

        /* Return a list of Users with username that contains the keyword
         * Case-insensitive match
         */
        public static List<User> GetUsersByKeyword(string keyword)
        {
            List<User> userList = new();

            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "SELECT [User].Id AS Id, Username, Email, [UserProfile].Profile " +
                        "FROM [User] JOIN [UserProfile] ON [User].Profile = [UserProfile].Id " +
                        "WHERE Username COLLATE SQL_Latin1_General_CP1_CI_AS LIKE @keyword;";
                    var result = db.Query(sql, new { keyword = "%" + keyword + "%" });
                    foreach (var row in result)
                    {
                        UserProfile tempProfile = new UserProfile() { Profile = row.Profile };
                        User user = new User()
                        {
                            Id = row.Id,
                            Username = row.Username,
                            Email = row.Email,
                            Profile = tempProfile
                        };
                        userList.Add(user);
                    }
                }
            }
            catch { }

            return userList;
        }

        /* Update User information
         * changePW indicate whether we want to change password. 
         * If changePW=false, value of pw will be ignored
         */
        public static User UpdateUserInfo(string uNameOld, string uNameNew, string pw,
            string email, bool changePW)
        {
            User updatedUser = new User()
            {
                Username = uNameNew,
                Password = "",
                Email = email
            };

            try
            {
                using (var db = DbConnection.Open())
                {
                    if (changePW)
                    {
                        string sql = "UPDATE [User] SET Username=@uNameNew, Email=@email, " +
                            "Password = ( HASHBYTES('SHA2_512', CONVERT(VARCHAR(50),@password) ) ) " +
                            "WHERE Username=@uNameOld;";

                        var param = new
                        {
                            uNameNew = uNameNew,
                            email = email,
                            password = pw,
                            uNameOld = uNameOld
                        };

                        db.Query(sql, param);
                    }
                    else
                    {
                        string sql = "UPDATE [User] SET Username=@uNameNew, Email=@email " +
                            "WHERE Username=@uNameOld;";

                        var param = new
                        {
                            uNameNew = uNameNew,
                            email = email,
                            uNameOld = uNameOld
                        };

                        db.Query(sql, param);
                    }

                }
            }
            catch { updatedUser = null; }

            //Return user after update
            return updatedUser;
        }

        /* Update User information
         * changePW indicate whether we want to change password. 
         * If changePW=false, value of pw will be ignored
         */
        public static User UpdateUserInfo(string uNameOld, string uNameNew, string pw, 
            string email, string profile, bool changePW)
        {
            User updatedUser = new User()
            {
                Username = uNameNew,
                Password = "",
                Email = email,
                Profile = new UserProfile() { Profile = profile }
            };

            try
            {
                using (var db = DbConnection.Open())
                {
                    if (changePW)
                    {
                        string sql = "UPDATE [User] SET Username=@uNameNew, Email=@email, " +
                            "Profile=(SELECT Id FROM [UserProfile] WHERE Profile=@profile), " +
                            "Password = ( HASHBYTES('SHA2_512', CONVERT(VARCHAR(50),@password) ) ) " +
                            "WHERE Username=@uNameOld;";

                        var param = new
                        {
                            uNameNew = uNameNew,
                            email = email,
                            profile = profile,
                            password = pw,
                            uNameOld = uNameOld
                        };

                        db.Query(sql, param);
                    }
                    else
                    {
                        string sql = "UPDATE [User] SET Username=@uNameNew, Email=@email, " +
                            "Profile=(SELECT Id FROM [UserProfile] WHERE Profile=@profile) " +
                            "WHERE Username=@uNameOld;";

                        var param = new
                        {
                            uNameNew = uNameNew,
                            email = email,
                            profile = profile,
                            uNameOld = uNameOld
                        };

                        db.Query(sql, param);
                    }

                }
            }
            catch { updatedUser = null; }

            //Return user after update
            return updatedUser;
        }

        public static bool AddNewUser(string username, string password, string profile, string email)
        {
            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "INSERT INTO [User] (Username, Password, Email, Profile) VALUES (@username, " +
                        "( HASHBYTES('SHA2_512', CONVERT(VARCHAR(50),@password) ) ), @email, (SELECT Id FROM [UserProfile] " +
                        "WHERE Profile=@profile) );";
                    var param = new { username, password, email, profile };
                    db.Query(sql, param);
                }
            }
            catch { return false; }
            return true;
        }
    }
}

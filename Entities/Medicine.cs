﻿using AimingForHD.Helpers;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace AimingForHD.Entities
{
    [Table("Medicine")]
    public class Medicine
    {
        [Key]
        public int MedicineId { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }

        public int PrescriptionId { get; set; }

        public static List<Medicine> GetMedicinesByPrescription(int prescriptionId)
        {
            List<Medicine> medicines = null;
            using(var db = DbConnection.Open())
            {
                string sql = "SELECT MedicineId, Name, Amount, PrescriptionId " +
                    "FROM Medicine WHERE PrescriptionId = @prescriptionId;";
                medicines = db.Query<Medicine>(sql, new { prescriptionId }).AsList();
            }
            return medicines;
        }

        public static Medicine AddNewMedicine(string name, int amount, int prescriptionId)
        {
            Medicine newMed = null;
            try {
                using var db = DbConnection.Open();
                string sql = "INSERT INTO Medicine (Name, Amount, PrescriptionId) OUTPUT INSERTED.MedicineId, " +
                    "INSERTED.Name, INSERTED.Amount, INSERTED.PrescriptionId VALUES (@name, @amount, @prescriptionId)";
                var insert = db.QuerySingle(sql, new { name, amount, prescriptionId });
                newMed = new Medicine()
                {
                    MedicineId = insert.MedicineId,
                    Name = insert.Name,
                    Amount = insert.Amount,
                    PrescriptionId = insert.PrescriptionId
                };
            } catch { return null; }

            return newMed;
        }

        public static Medicine GetMed(int medicineId)
        {
            Medicine medicine = null;
            try
            {
                using var db = DbConnection.Open();
                medicine = db.Get<Medicine>(medicineId);
            } catch { return null; }
            return medicine;
        }

        /* Return null if update fails
         */
        public static Medicine UpdateMed(int medicineId, string name, int amount)
        {
            Medicine medicine = null;

            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "UPDATE Medicine SET Name=@name, Amount=@amount " +
                        "OUTPUT INSERTED.MedicineId, INSERTED.Name, INSERTED.Amount, INSERTED.PrescriptionId " +
                        "WHERE MedicineId=@medicineId";
                    medicine = db.QuerySingle<Medicine>(sql, new { medicineId, name, amount });
                }
            }
            catch { return null; }

            return medicine;
        }

        /* Return null if update fails
         */
        public static Medicine DispenseMed(int medicineId, int amount)
        {
            Medicine medicine = null;

            try
            {
                using (var db = DbConnection.Open())
                {
                    string sql = "UPDATE Medicine SET Amount=Amount-@amount " +
                        "OUTPUT INSERTED.MedicineId, INSERTED.Name, INSERTED.Amount, INSERTED.PrescriptionId " +
                        "WHERE MedicineId=@medicineId";
                    medicine = db.QuerySingle<Medicine>(sql, new { medicineId, amount });

                }
            }
            catch
            {
                try
                {
                    return DbConnection.Open().Get<Medicine>(medicineId);
                }
                catch { return null; }
            }

            return medicine;
        }
    }
}
